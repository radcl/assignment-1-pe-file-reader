var structSection__header =
[
    [ "Characteristics", "structSection__header.html#a2db2d9b50cd35cc06d223fe021daaf04", null ],
    [ "name", "structSection__header.html#a33280ba911de12b8bbd726e7aa9d5c93", null ],
    [ "NumberOfLineNumbers", "structSection__header.html#ac553861c865aec02ee80ad569e556d9c", null ],
    [ "NumberOfRelocations", "structSection__header.html#a14662eb7233065c3c749703cbb494c19", null ],
    [ "PointerToLineNumbers", "structSection__header.html#abb8cd7060c93554319a06100d7aafee4", null ],
    [ "PointerToRawData", "structSection__header.html#a5a81028801ea48dab69972b726977fa8", null ],
    [ "PointerToRelocations", "structSection__header.html#af5ce14e48baf5656f890dfb7c3f488a9", null ],
    [ "SizeOfRawData", "structSection__header.html#a7489ede56b9c87ae753ef500cc27ad0b", null ],
    [ "VirtialSize", "structSection__header.html#a6dafb305d0d30cf20131de9c077eafff", null ],
    [ "VirtualAddress", "structSection__header.html#a9654185e0d962a473a9b46e4c0fe1dcb", null ]
];