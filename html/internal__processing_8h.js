var internal__processing_8h =
[
    [ "search_result", "structsearch__result.html", "structsearch__result" ],
    [ "extract_section_result", "internal__processing_8h.html#acd5a8574c523b104aaa98a4a7658d20b", [
      [ "EXTRACTION_OK", "internal__processing_8h.html#acd5a8574c523b104aaa98a4a7658d20ba07855f3e8fd336d72141f59f6e379cd9", null ],
      [ "EXTRACTION_READ_HEADER_ERROR", "internal__processing_8h.html#acd5a8574c523b104aaa98a4a7658d20baf988c8ab2f92108db2d389b1a3d840c2", null ],
      [ "EXTRACTION_READ_SECTION_ERROR", "internal__processing_8h.html#acd5a8574c523b104aaa98a4a7658d20baa36231aa1b6a7c809293606317df9833", null ],
      [ "EXTRACTION_FILE_ERROR", "internal__processing_8h.html#acd5a8574c523b104aaa98a4a7658d20bacd7d0c7f910ef162d4252f2e9fa166f9", null ]
    ] ],
    [ "read_header_result", "internal__processing_8h.html#a1480caf8c835efd912594d3c43144910", [
      [ "READ_HEADER_OK", "internal__processing_8h.html#a1480caf8c835efd912594d3c43144910aa46e469699fcb7520e28ef6ad554ccb5", null ],
      [ "READ_HEADER_ERROR", "internal__processing_8h.html#a1480caf8c835efd912594d3c43144910a460a8b5a5e390a22850ee93071b543f6", null ]
    ] ],
    [ "extract_section", "internal__processing_8h.html#a951e6dbd224bbb75c5a8a36542490769", null ],
    [ "read_header", "internal__processing_8h.html#a2bbf1172798c6ff08bc931b69e1e26f8", null ]
];