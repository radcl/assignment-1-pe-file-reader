var structPEHeader =
[
    [ "Characteristics", "structPEHeader.html#a5b823ed42dd5f0fc7b62203d170ea1eb", null ],
    [ "Machine", "structPEHeader.html#a24e289a20d9ed1becaa5d20a4fb265ca", null ],
    [ "magic", "structPEHeader.html#a67719b681c02365c7262bd0805ef1e07", null ],
    [ "NumberOfSections", "structPEHeader.html#a2f30155c9a4c054d5c9030b7af2902d7", null ],
    [ "NumberOfSymbols", "structPEHeader.html#acc70d304f460fd41b68616c5581bfa17", null ],
    [ "PointerToSymbolTable", "structPEHeader.html#a4bd49937405075d9974939d40d118512", null ],
    [ "SizeOfOptionalHeader", "structPEHeader.html#a1d2ae31fd68935846b59777004b60ca2", null ],
    [ "TimeDateStamp", "structPEHeader.html#a34e227ed3cd01a2e35e0078830589590", null ]
];