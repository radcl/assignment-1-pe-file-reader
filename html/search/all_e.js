var searchData=
[
  ['read_5fheader_64',['read_header',['../internal__processing_8h.html#a2bbf1172798c6ff08bc931b69e1e26f8',1,'read_header(FILE *from, struct PEHeader *dest):&#160;internal_processing.c'],['../internal__processing_8c.html#a2bbf1172798c6ff08bc931b69e1e26f8',1,'read_header(FILE *from, struct PEHeader *dest):&#160;internal_processing.c']]],
  ['read_5fheader_5ferror_65',['READ_HEADER_ERROR',['../internal__processing_8h.html#a1480caf8c835efd912594d3c43144910a460a8b5a5e390a22850ee93071b543f6',1,'internal_processing.h']]],
  ['read_5fheader_5fok_66',['READ_HEADER_OK',['../internal__processing_8h.html#a1480caf8c835efd912594d3c43144910aa46e469699fcb7520e28ef6ad554ccb5',1,'internal_processing.h']]],
  ['read_5fheader_5fresult_67',['read_header_result',['../internal__processing_8h.html#a1480caf8c835efd912594d3c43144910',1,'internal_processing.h']]],
  ['read_5fonly_68',['READ_ONLY',['../file__processing_8h.html#aa6a9be7e20c1d1d4ac2dfa1266fa78efaec889f8b3140e20b857e18ccd267e049',1,'file_processing.h']]],
  ['readme_2emd_69',['README.md',['../README_8md.html',1,'']]],
  ['result_70',['result',['../structopen__attempt.html#a6177aa3f8595d9613e013420b0c5e395',1,'open_attempt::result()'],['../structsearch__result.html#a9ca156d4d85d915ff18f7d7d3c17a06c',1,'search_result::result()']]]
];
