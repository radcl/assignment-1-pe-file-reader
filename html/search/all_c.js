var searchData=
[
  ['open_5fattempt_52',['open_attempt',['../structopen__attempt.html',1,'']]],
  ['open_5ferror_53',['OPEN_ERROR',['../file__processing_8h.html#af1b74a8fbca9227e97bc9be072e6ad72a10bfe36115b5906a85ed387f04fa6bdf',1,'file_processing.h']]],
  ['open_5ffile_54',['open_file',['../file__processing_8h.html#a3504434677b9896e0f54492335bea0b6',1,'open_file(const char *path, const char *rights):&#160;file_processing.c'],['../file__processing_8c.html#a3504434677b9896e0f54492335bea0b6',1,'open_file(const char *path, const char *rights):&#160;file_processing.c']]],
  ['open_5fok_55',['OPEN_OK',['../file__processing_8h.html#af1b74a8fbca9227e97bc9be072e6ad72a430d4c7587b132b2d47db1f9b28fbcc2',1,'file_processing.h']]],
  ['open_5fresult_56',['open_result',['../file__processing_8h.html#af1b74a8fbca9227e97bc9be072e6ad72',1,'file_processing.h']]],
  ['open_5frights_57',['open_rights',['../file__processing_8h.html#aa6a9be7e20c1d1d4ac2dfa1266fa78ef',1,'file_processing.h']]]
];
