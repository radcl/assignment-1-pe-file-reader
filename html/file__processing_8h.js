var file__processing_8h =
[
    [ "open_attempt", "structopen__attempt.html", "structopen__attempt" ],
    [ "close_result", "file__processing_8h.html#aab6c025e3e1f240f95336a35298dec99", [
      [ "CLOSE_OK", "file__processing_8h.html#aab6c025e3e1f240f95336a35298dec99ae49e38cb647fc392bf5dd83664e5310f", null ],
      [ "CLOSE_ERROR", "file__processing_8h.html#aab6c025e3e1f240f95336a35298dec99a767ddbbf9a51692f8e550d75600184bc", null ]
    ] ],
    [ "open_result", "file__processing_8h.html#af1b74a8fbca9227e97bc9be072e6ad72", [
      [ "OPEN_OK", "file__processing_8h.html#af1b74a8fbca9227e97bc9be072e6ad72a430d4c7587b132b2d47db1f9b28fbcc2", null ],
      [ "OPEN_ERROR", "file__processing_8h.html#af1b74a8fbca9227e97bc9be072e6ad72a10bfe36115b5906a85ed387f04fa6bdf", null ]
    ] ],
    [ "open_rights", "file__processing_8h.html#aa6a9be7e20c1d1d4ac2dfa1266fa78ef", [
      [ "READ_ONLY", "file__processing_8h.html#aa6a9be7e20c1d1d4ac2dfa1266fa78efaec889f8b3140e20b857e18ccd267e049", null ],
      [ "WRITE_ONLY", "file__processing_8h.html#aa6a9be7e20c1d1d4ac2dfa1266fa78efac8a37834e5c8e41f9f9688501f07770e", null ]
    ] ],
    [ "close_file", "file__processing_8h.html#a272978cde81f5a040df2a94d2e12ddc1", null ],
    [ "open_file", "file__processing_8h.html#a3504434677b9896e0f54492335bea0b6", null ]
];