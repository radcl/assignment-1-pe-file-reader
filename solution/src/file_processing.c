/// @file
/// @brief File with file operations
#include "../include/file_processing.h"
#include "stdio.h"

/// @brief Function that tries to open the file
/// @param[in] path File path
/// @param[in] rights Open mode
/// @return open appempt structure
struct open_attempt open_file(const char* path, const char* rights) {
    FILE* output_file = fopen(path, rights);
    if (output_file) return (struct open_attempt) { .file = output_file, .result = OPEN_OK };
    else {
        fprintf(stderr, "%s", "An error during opening file!");
        return (struct open_attempt) { .file = NULL, .result = OPEN_ERROR };
    }
}

/// @brief Function that tries to close the file
/// @param[in] file File handle
/// @return CLOSE_OK if file closed successfully
enum close_result close_file(FILE* file) {
    if (fclose(file)) {
        fprintf(stderr, "%s", "An error during closing file!");
        return CLOSE_ERROR;
    }
    return CLOSE_OK;
}
