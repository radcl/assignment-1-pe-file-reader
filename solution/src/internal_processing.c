/// @file
/// @brief File with internal format operations
#include "../include/file_processing.h"
#include "../include/internal_format.h"
#include "../include/internal_processing.h"
#include "string.h"
#include <malloc.h>
#include <stdbool.h>
#include <stdio.h>

/// DOS offset
const uint64_t DOS_OFFSET = 0x3C; 
/// Offset to first section
const uint64_t FIRST_SECTION_OFFSET = 0xF0;
/// Offset between two sections
const uint64_t SECTIONS_OFFSET = 0x28;


/// @brief The function that calculates offset to PEHeader
/// @param source The input file handle
/// @return Offset to PEHeader
static uint32_t get_start_offset(FILE* source) {
    uint32_t offset = 0; // Offset that will be returned
    fseek(source, (long) DOS_OFFSET, SEEK_SET);

    if (!fread(&offset, 4, 1, source)) return -1;

    fseek(source, 0, SEEK_SET);
    return offset;
}

/// @brief Function to skip offset in file handle
/// @param source Input file handle
/// @param offset Offset that will be skipped
/// @return True if skip was successful
static bool skip_offset(FILE* source, uint32_t offset) {
    return !fseek(source, offset, SEEK_CUR);
}

/// @brief Function that tries to find section header by section name
/// @param source Input file handle
/// @param section_name Necessary section name
/// @param limit Number of sections
/// @return Search result struct
static struct search_result find_section(FILE* source, const char* section_name, uint16_t limit) {
    // The name of current section we are checking right now
    char curr_section[8] = {0}; 
    // The number of checked sections
    uint16_t total_checked = 0;
    // The result of current and target section name comparing
    int compare_result = strncmp(section_name, (const char*) curr_section, 8);

    // Search cycle
    while (compare_result && (total_checked <= limit)) {
        fread(&curr_section, 8, 1, source);
        skip_offset(source, (long) SECTIONS_OFFSET - 8);
        compare_result = strncmp(section_name, (const char*) curr_section, 8);
        total_checked++;
    }

    // Output section header
    struct Section_header output_header = {0};

    // Return result that section wasn't found
    if (total_checked > limit) return (struct search_result) { .header = output_header, .result = SHR_NOT_FOUND };
    else {
        fseek(source, (long) -SECTIONS_OFFSET, SEEK_CUR);
        fread(&output_header, sizeof(struct Section_header), 1, source);
        return (struct search_result) { .header = output_header, .result = SHR_FOUND };
    }
    
}

/// @brief Function that reads PEHeader
/// @param[in] from Input PE file handle
/// @param[in] dest Destination of header reading
/// @return READ_HEADER_OK if reading was successful
enum read_header_result read_header(FILE* from, struct PEHeader* dest) {
    const uint32_t start_offset = get_start_offset(from);
    if (start_offset < 0) return READ_HEADER_ERROR;

    skip_offset(from, get_start_offset(from));
    fread(dest, sizeof(struct PEHeader), 1, from);
    return READ_HEADER_OK;
}

/// @brief Function that extracts section raw data
/// @param[in] source Input PE file handle
/// @param[in] section_name Necessary section name
/// @param[in] to Destination file handle
enum extract_section_result extract_section(FILE* source, const char* section_name, FILE* to) {
    // Skip DOS offset
    skip_offset(source, DOS_OFFSET);

    // Input PE file header
    struct PEHeader header;
    // Result of header reading attempt
    enum read_header_result result = read_header(source, &header);

    // Exit if something went wrong
    if (result != READ_HEADER_OK) return EXTRACTION_READ_HEADER_ERROR;

    // Skip offset to get started for section search
    skip_offset(source, FIRST_SECTION_OFFSET);
    // Try to find necessary section
    struct search_result search_res = find_section(source, section_name, header.NumberOfSections);
    if (result == SHR_NOT_FOUND) return EXTRACTION_READ_SECTION_ERROR;
    else {
        // Necessary section header
        struct Section_header section_header = search_res.header;
        // Output raw data size
        const uint32_t NECESSARY_SIZE = section_header.SizeOfRawData;
        // Return error if smth went wrong
        if (!NECESSARY_SIZE) return EXTRACTION_READ_HEADER_ERROR;

        // Output data
        uint32_t* data = calloc(NECESSARY_SIZE, 1);

        // Write output data and close destination file
        fseek(source, section_header.PointerToRawData, SEEK_SET);
        fread(data, NECESSARY_SIZE, 1, source);
        fwrite(data, section_header.SizeOfRawData, 1, to);
        free(data);
        
        // Check is closing was successful
        if (close_file(to) != CLOSE_OK) return EXTRACTION_FILE_ERROR;
    }

    return EXTRACTION_OK;
}
