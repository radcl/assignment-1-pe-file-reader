/// @file 
/// @brief Main application file

#include "../include/file_processing.h"
#include "../include/internal_format.h"
#include "../include/internal_processing.h"
#include "string.h"
#include <stdio.h>

/// Application name string
#define APP_NAME "section-extractor"

const char* const str_open_rigths[] = {
    [READ_ONLY] = "rt",
    [WRITE_ONLY] = "wb"
};

const char* const extraction_result_messages[] = {
  [EXTRACTION_OK] = "Extraction completed successfully.",
  [EXTRACTION_READ_HEADER_ERROR] = "An error during reading header!",
  [EXTRACTION_READ_SECTION_ERROR] = "An error during reading section!"
};


/// @brief Application entry point
/// @param[in] argc Number of command line arguments
/// @param[in] argv Command line arguments
/// @return 0 in case of success or error code
int main(int argc, char** argv)
{
  (void) argc; (void) argv; // supress 'unused parameters' warning

  // Try to open input exe file
  struct open_attempt input_attempt = open_file(argv[1], str_open_rigths[READ_ONLY]);
  // Return -1 if smth went wrong
  if (input_attempt.result != OPEN_OK) return -1;

  // Necessary section name
  const char* section_name = argv[2];
  
  // Try to open output file
  struct open_attempt output_attempt = open_file(argv[3], str_open_rigths[WRITE_ONLY]);
  // Return -1 if smth went wrong
  if (output_attempt.result != OPEN_OK) return -1;

  // Check result of section extraction
  enum extract_section_result result = extract_section(input_attempt.file, (const char* ) section_name, output_attempt.file);
  if (result != EXTRACTION_OK) {
    fprintf(stderr, "%s", extraction_result_messages[result]);
    return -1;
  }

  fprintf(stdout, "%s", extraction_result_messages[result]);
  return 0;
}
