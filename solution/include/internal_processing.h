#ifndef INTERNAL_PROCESSING
#define INTERNAL_PROCESSING

#include "internal_format.h"
#include "stdio.h"

/// Enum that contains PE header reading results
enum read_header_result {
    /// OK code
    READ_HEADER_OK,
    /// Error code
    READ_HEADER_ERROR
};

/// Enum that contains PE section extracting results 
enum extract_section_result {
    /// Extraction OK code
    EXTRACTION_OK,
    /// Reading header error code
    EXTRACTION_READ_HEADER_ERROR,
    /// Reading section error code
    EXTRACTION_READ_SECTION_ERROR,
    /// Reading/writing file error code
    EXTRACTION_FILE_ERROR
};

/// Structure for protected section searching
struct search_result {
    /// Section header structure
    struct Section_header header;
    /// Search result code
    enum { 
        /// Section found successfully code
        SHR_FOUND, 
        /// Section not found code
        SHR_NOT_FOUND } result;
};

enum read_header_result read_header(FILE* from, struct PEHeader* dest);
enum extract_section_result extract_section(FILE* source, const char* section_name, FILE* to);

#endif // INTERNAL_PROCESSING
