#ifndef INTERNAL_FORMAT_H
#define INTERNAL_FORMAT_H

#include "inttypes.h"

/// PE file header
#ifdef _MSC_VER
#pragma pack(push, 1)
#endif // _MSC_VER
struct
#ifdef __GNUC__
__attribute__((packed))
#endif // __GNUC__
PEHeader {
    /// Magic PE word
    uint8_t magic[4];
    /// Type of target computer
    uint16_t Machine;
    /// The number of sections
    uint16_t NumberOfSections;
    /// When the file was created
    uint32_t TimeDateStamp;
    /// The offset to symbol table file
    uint32_t PointerToSymbolTable;
    /// The number of entries in the symbol table
    uint32_t NumberOfSymbols;
    /// The size of an optional header
    uint16_t SizeOfOptionalHeader;
    /// Flags
    uint16_t Characteristics;
};
#if defined _MSC_VER
#pragma pack(pop)
#endif // _MSC_VER

/// PE file section header structure
#ifdef _MSC_VER
#pragma pack(push, 1)
#endif // _MSC_VER
struct
#ifdef __GNUC__
__attribute__((packed))
#endif // __GNUC__
Section_header {
    /// Section name
    uint8_t name[8];
    /// The total size of partition
    uint32_t VirtialSize;
    /// The address of first byte of section
    uint32_t VirtualAddress;
    /// Partition size
    uint32_t SizeOfRawData;
    /// Pointer to the first page of a section
    uint32_t PointerToRawData;
    /// A pointer to the beginning of the move records
    uint32_t PointerToRelocations;
    /// A pointer to the beginning of the line number entries
    uint32_t PointerToLineNumbers;
    /// The number of move entries
    uint16_t NumberOfRelocations;
    /// The number of row number entries
    uint16_t NumberOfLineNumbers;
    /// Flags
    uint32_t Characteristics;
};
#ifdef _MSC_VER
#pragma pack(pop)
#endif // _MSC_VER

#endif // INTERNAL_FORMAT_H
