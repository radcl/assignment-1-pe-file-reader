/// @file
/// @brief 
#ifndef FILE_PROCESSING_H
#define FILE_PROCESSING_H
#include <stdio.h>

/// Enum that contains file opening results
enum open_result {
    /// OK code
    OPEN_OK,
    /// Error code
    OPEN_ERROR
};

/// Enum that contain file closing results
enum close_result {
    /// OK code
    CLOSE_OK,
    /// Error code
    CLOSE_ERROR
};

/// Enum that contains open modes
enum open_rights {
    /// OK code
    READ_ONLY,
    /// Error code
    WRITE_ONLY
};

/// Structure for protected file opening
struct open_attempt {
    /// File handle
    FILE* file;
    /// Result code
    enum open_result result;
};

struct open_attempt open_file(const char* path, const char* rights);
enum close_result close_file(FILE* file);

#endif // FILE_PROCESSING_H
